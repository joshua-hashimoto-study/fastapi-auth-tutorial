# fastapi-auth-tutorial

## what I used

A video from a youtuber Pretty Printed.

video link:
https://www.youtube.com/watch?v=6hTRw_HK3Ts

## docker-compose.yml

```yml
version: "3"

services: 
    api:
        build: .
        command: uvicorn app.main:app --reload --host 0.0.0.0 --port 8000
        environment: 
            APPLICATION_NAME: fastapi-docker-template
            ENVIRONMENT: development
            SECRET_KEY: your secret key
            DEBUG: 1
            API_VERSION: 1
        volumes: 
            - .:/app
        ports: 
            - 8000:8000
        depends_on:
            - db
    db:
        image: postgres:11
        volumes:
            - postgres_data:/var/lib/postgresql/data/
        environment:
            - "POSTGRES_HOST_AUTH_METHOD=trust"

volumes:
    postgres_data:
```

## working images

![generate_token](./readme/images/generate_token.png)

![get_user_by_jwt](./readme/images/get_user_by_jwt.png)
