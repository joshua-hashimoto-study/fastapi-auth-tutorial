from fastapi import Request
from fastapi.responses import JSONResponse

from .exception_classes import PageException


def page_exception_hander(_: Request, exc: PageException):
    status_code = 400
    context = {
        'errorMessage': exc.message,
    }
    return JSONResponse(status_code=status_code, content=context)
