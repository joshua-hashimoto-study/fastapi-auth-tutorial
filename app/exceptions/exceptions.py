from fastapi import FastAPI

from .exception_classes import PageException
from .exception_handlers import page_exception_hander


def set_exception_handers(app: FastAPI):
    app.add_exception_handler(
        PageException,
        page_exception_hander,
    )
