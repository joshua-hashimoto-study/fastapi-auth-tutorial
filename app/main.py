from fastapi import FastAPI

from .exceptions import set_exception_handers
from .middlewares import set_middleware
from .routers import set_routers
from .models import setup_database

app = FastAPI(
    title='FastAPI+Docker template',
    description='API template for FastAPI+Docker',
    version='0.1.0',
)


# configs
set_middleware(app)
set_exception_handers(app)
set_routers(app)
setup_database(app)


# uvicorn app.main:app --reload
