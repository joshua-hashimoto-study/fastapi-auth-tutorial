from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise


def setup_database(app: FastAPI):
    register_tortoise(
        app,
        db_url=get_db_uri(
            user='postgres',
            password='postgres',
            host='db',  # docker-composeのservice名
            db='postgres',
        ),
        modules={'models': ['app.models.auth_models']},
        generate_schemas=True,
        add_exception_handlers=True,
    )


def get_db_uri(*, user, password, host, db):
    return f'postgres://{user}:{password}@{host}:5432/{db}'


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql',
#         'NAME': 'postgres',
#         'USER': 'postgres',
#         'PASSWORD': 'postgres',
#         'HOST': 'db',  # docker-composeのservice
#         'PORT': 5432
#     }
# }
