from fastapi import FastAPI

from .auth import set_auth_router
from .example import set_example_router


def set_routers(app: FastAPI):
    # set_example_router(app)
    set_auth_router(app)
