import os
from typing import Optional

import jwt
from fastapi import FastAPI, APIRouter, Path, Depends, Query, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from passlib.hash import bcrypt

from app.models.auth_models import User, UserIn_Pydantic, User_Pydantic
from app.resources.constants import BASE_API_URL


router = APIRouter()

app_name = 'auth'

oauth2_schema = OAuth2PasswordBearer(tokenUrl=f'{BASE_API_URL}/token')

SECRET_KEY = os.environ.get('SECRET_KEY')


def set_auth_router(app: FastAPI):
    app.include_router(
        router,
        prefix=f'/{BASE_API_URL}',
        tags=[app_name]
    )


async def authenticate_user(username: str, password: str):
    user = await User.get(username=username)
    if not user:
        return False
    if not user.verify_password(password):
        return False
    return user


@router.post('/token')
async def generate_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = await authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail='Invalid username or password')
    user_obj = await User_Pydantic.from_tortoise_orm(user)
    # this includes the hashed password. in production, you would want to remote that.
    token = jwt.encode(user_obj.dict(), SECRET_KEY)
    # if you are using OAuth2 you need to return access_token
    return {'access_token': token, 'token_type': 'bearer'}


@router.get('/')
async def index(token: str = Depends(oauth2_schema)):
    return {'the_token': token}


@router.post('/users', response_model=User_Pydantic)
async def create_user(user: UserIn_Pydantic):
    user_obj = User(username=user.username,
                    password_hash=bcrypt.hash(user.password_hash))
    await user_obj.save()
    return await User_Pydantic.from_tortoise_orm(user_obj)


async def get_current_user(token: str = Depends(oauth2_schema)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=['HS256'])
        user = await User.get(id=payload.get('id'))
    except:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail='Invalid username or password')

    return await User_Pydantic.from_tortoise_orm(user)


@router.get('/users/me', response_model=User_Pydantic)
async def get_user(user: User_Pydantic = Depends(get_current_user)):
    # to call this include JWT token to "Authentication" in your request.
    # that token will be passed to "get_current_user" function that is depending on
    # and check if user can be fetched from the database
    return user
