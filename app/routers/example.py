from typing import Optional

from fastapi import FastAPI, APIRouter, Path, Depends, Query
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

from app.resources.constants import BASE_API_URL


router = APIRouter()

app_name = 'example'

oauth2_schema = OAuth2PasswordBearer(tokenUrl=f'{BASE_API_URL}/token')


def set_example_router(app: FastAPI):
    app.include_router(
        router,
        prefix=f'/{BASE_API_URL}',
        tags=[app_name]
    )


@router.post('/token')
async def token(form_data: OAuth2PasswordRequestForm = Depends()):
    # if you are using OAuth2 you need to return access_token
    return {'access_token': form_data.username + 'token'}


@router.get('/')
async def index(token: str = Depends(oauth2_schema)):
    return {'the_token': token}
